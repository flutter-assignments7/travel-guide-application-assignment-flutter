import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class TravelGuideElevatedButton extends StatelessWidget {
  final Color primaryColor;
  final double buttonHeight;
  // final double buttonWidth;
  final String buttonText;
  final double fontSize;
  final VoidCallback? onPressed;

  TravelGuideElevatedButton({
    this.primaryColor = Colors.transparent,
    required this.buttonHeight,
    // required this.buttonWidth,
    required this.buttonText,
    required this.fontSize,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(

      style: ElevatedButton.styleFrom(
        primary: primaryColor ,
        minimumSize: Size(
          double.infinity,
          // SizeConfig.scaleWidth(buttonWidth),
          SizeConfig.scaleHeight(buttonHeight),
        ),

        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            SizeConfig.scaleWidth(20),
          ),
        ),
      ),
      onPressed: onPressed,
      child: TravelGuideText(
        text: buttonText,
        fontSize: SizeConfig.scaleTextFont(fontSize),
        fontWeight: FontWeight.w600   ,
        textColor: Colors.white,
      ),
    );
  }
}
