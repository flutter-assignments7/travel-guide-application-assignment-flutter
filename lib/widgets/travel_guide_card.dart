import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class TravelGuideCard extends StatelessWidget {
  final Widget? leadingIcon;
  final Widget? trailingIcon;
  final String? title;
  final String? titleName;
  final String subTitle;
  final VoidCallback? pushTile;

  TravelGuideCard({
    this.leadingIcon,
    this.trailingIcon,
    this.title,
    this.titleName,
    this.subTitle = "",
    this.pushTile
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: Colors.transparent,
      child: ListTile(
        onTap: pushTile,
        contentPadding: EdgeInsets.all(0),
        leading: leadingIcon,
        title: RichText(
          text: TextSpan(
            text: title,
            style: TextStyle(
              fontSize: SizeConfig.scaleTextFont(18),
              fontFamily: 'Poppins',
              color: Colors.black,
            ),
            children: [
              TextSpan(
                text: titleName,
                style: TextStyle(
                  fontSize: SizeConfig.scaleWidth(18),
                  fontFamily: 'Poppins',
                  color: Color(0xFFFFA183),
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
        subtitle: TravelGuideText(
          text: subTitle,
          fontSize: 13,
          textColor: Color(0xFFB7B7B7),
        ),
        trailing: trailingIcon,


      ),
    );
  }
}
