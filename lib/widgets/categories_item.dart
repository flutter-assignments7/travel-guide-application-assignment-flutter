
import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class CategoriesItem extends StatelessWidget {
  final String title;
  final String subtitle;
  final VoidCallback? onTapGoToCategory;
  final Widget? leadingIcon;
  final double containerHeight;

  CategoriesItem({
    required this.title,
    required this.subtitle,
    required this.onTapGoToCategory,
    required this.leadingIcon,
    this.containerHeight = 110,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: SizeConfig.scaleHeight(containerHeight),
      width: SizeConfig.scaleWidth(335),
      margin: EdgeInsetsDirectional.only(
        bottom: SizeConfig.scaleHeight(10),
      ),
      decoration: BoxDecoration(
        // color:  ? Color(0xFFFFF4F2) : Colors.white,
        borderRadius: BorderRadius.circular(25),
        border: Border.all(
          color: Color(0xFFF8F7FE),
          width: 1,
        ),
      ),
      child: ListTile(
        onTap: onTapGoToCategory,
        tileColor: Colors.white,
        // contentPadding: EdgeInsetsDirectional.only(top: 11, bottom: 11, start: 15),
        leading: leadingIcon,
        title: TravelGuideText(
          text: title,
          fontSize: SizeConfig.scaleTextFont(13),
          fontWeight: FontWeight.w500,
          fontFamily: 'Quicksand',
        ),
        subtitle: TravelGuideText(
          text: subtitle,
          fontSize: SizeConfig.scaleTextFont(12),
          fontWeight: FontWeight.w500,
          fontFamily: 'Quicksand',
          textColor: Color(0xFFA5A5A5),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          color: Color(0xFFB7B7B7),
        ),
      ),
    );
  }
}
