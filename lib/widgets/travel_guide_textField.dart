import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class TravelGuideTextField extends StatelessWidget {
  final TextEditingController? textEditingController;
  final String? textError;
  final bool obscureText;
  final TextInputType keyboardType;
  final String hintTextField;
  final double borderWidth;
  final double hintFontSize;
  final FontWeight hintFontWeight;
  final String hintFontFamily;
  final double textSize;
  final Widget? suffixIcon;
  final Color fillColor;
  final double paddingTop;
  final double paddingEnd;
  final double paddingBottom;

  TravelGuideTextField({
    this.obscureText = false,
    this.keyboardType = TextInputType.text,
    required this.hintTextField,
    this.borderWidth = 1,
    this.hintFontSize = 15,
    this.hintFontWeight = FontWeight.w300,
    this.hintFontFamily = 'Poppins',
    this.textEditingController,
    this.textError,
    this.textSize = 15,
    this.suffixIcon,
    this.fillColor = Colors.white,
    this.paddingTop = 15,
    this.paddingBottom = 15,
    this.paddingEnd = 20,
  });


  @override
  Widget build(BuildContext context) {
    return TextField(
      cursorColor: Colors.black,
      cursorWidth: 1,
      controller: textEditingController,
      style: TextStyle(
        fontSize: textSize,
      ),
      obscureText: obscureText,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        fillColor: fillColor,
        filled: true,
        contentPadding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          top: SizeConfig.scaleHeight(paddingTop),
          bottom: SizeConfig.scaleHeight(paddingBottom),
          end: SizeConfig.scaleWidth(paddingEnd),
        ),
        errorText: textError,
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: SizeConfig.scaleWidth(1),
            color: Colors.red,
          ),
          borderRadius: BorderRadius.circular(
            SizeConfig.scaleWidth(20),
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: SizeConfig.scaleWidth(1),
            color: Colors.red,
          ),
          borderRadius: BorderRadius.circular(
            SizeConfig.scaleWidth(20),
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.transparent,
            width: SizeConfig.scaleWidth(1),
          ),
          borderRadius: BorderRadius.circular(
            SizeConfig.scaleWidth(20),
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFF9A9A9A),
            width: borderWidth,
          ),
          borderRadius: BorderRadius.circular(
            SizeConfig.scaleWidth(20),
          ),
        ),
        hintText: hintTextField,
        hintStyle: TextStyle(
          fontSize: hintFontSize,
          fontWeight: hintFontWeight,
          fontFamily: hintFontFamily,
          color: Colors.black,
        ),
        suffixIcon: suffixIcon,
      ),
    );
  }
}
