import 'package:flutter/material.dart';

class TravelGuideText extends StatelessWidget {
  final String text;
  final double fontSize;
  final FontWeight fontWeight;
  final String fontFamily;
  final Color textColor;
  final TextAlign textAlign;
  final TextDecorationStyle? decorationStyle;
  final TextDecoration? decoration;

  TravelGuideText({
    required this.text,
    this.fontSize = 16,
    this.fontWeight = FontWeight.normal,
    this.fontFamily = 'Poppins',
    this.textColor = Colors.black,
    this.textAlign = TextAlign.start,
    this.decorationStyle,
    this.decoration
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        fontFamily: fontFamily,
        color: textColor,
        decorationStyle: decorationStyle,
        decoration: decoration,
      ),
      textAlign: textAlign,


    );
  }
}
