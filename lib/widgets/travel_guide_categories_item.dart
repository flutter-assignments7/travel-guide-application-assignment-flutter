import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class TravelGuideCategoriesItem extends StatelessWidget {
  final String title;
  final String subTitle;
  final VoidCallback? onTap;
  final String pathImage;

  TravelGuideCategoriesItem({
    required this.title,
    required this.subTitle,
    required this.onTap,
    required this.pathImage,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: SizeConfig.scaleHeight(80),
      width: SizeConfig.scaleWidth(335),
      margin: EdgeInsetsDirectional.only(
        bottom: SizeConfig.scaleHeight(10),
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(25),
        border: Border.all(
          color: Color(0xFFF8F7FE),
          width: 5,
        ),
      ),
      child: ListTile(
        onTap: onTap,
        tileColor: Colors.transparent,
        // contentPadding: EdgeInsetsDirectional.only(top: 11, bottom: 11, start: 15),
        leading: CircleAvatar(
          backgroundImage: AssetImage(pathImage), //ratifications_university.jpg
          radius: 30,
        ),
        title: TravelGuideText(
          text: title,
          fontSize: SizeConfig.scaleTextFont(13),
          fontWeight: FontWeight.w500,
          fontFamily: 'Quicksand',
        ),
        subtitle: TravelGuideText(
          text: subTitle,
          fontSize: SizeConfig.scaleTextFont(12),
          fontWeight: FontWeight.w500,
          fontFamily: 'Quicksand',
          textColor: Color(0xFFA5A5A5),
        ),
        trailing: Icon(
          Icons.arrow_forward_ios,
          color: Color(0xFFB7B7B7),
        ),
      ),
    );
  }
}
