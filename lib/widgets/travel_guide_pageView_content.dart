import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class TravelGuidePageViewContent extends StatelessWidget {
  final String title;
  final String subTitle;
  final String pathImage;
  final double marginTopContainer1;
  final double marginTopContainer2;
  final double height;
  final String showDetails;
  final VoidCallback? ShowDetailsClick;

  TravelGuidePageViewContent({
    this.title = "",
    this.subTitle = "",
    required this.pathImage,
    required this.height,
    this.marginTopContainer1 = 0,
    this.marginTopContainer2 = 0,
    this.showDetails = "",
    this.ShowDetailsClick,
  });

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Stack(
      children: [
        Center(
          child: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig.scaleHeight(marginTopContainer1)),
            height: SizeConfig.scaleHeight(235),
            width: SizeConfig.scaleWidth(277),
            decoration: BoxDecoration(
              color: Color(0xFFF8F7FE),
              borderRadius: BorderRadius.circular(25),
              boxShadow: [
                BoxShadow(
                  // color:
                  color: Color(0xFF5B4DBC).withAlpha(50),
                  offset: Offset(0, 16),
                  blurRadius: 50,
                  spreadRadius: 0,
                ),
              ],
            ),
          ),
        ),
        Center(
          child: Container(
            margin: EdgeInsetsDirectional.only(
                top: SizeConfig.scaleHeight(marginTopContainer2)),
            height: SizeConfig.scaleHeight(height),
            width: SizeConfig.scaleWidth(300),
            decoration: BoxDecoration(
              color: Color(0xFFDFDBFC),
              borderRadius: BorderRadius.circular(25),
            ),
          ),
        ),
        Container(
          clipBehavior: Clip.antiAlias,
          height: SizeConfig.scaleHeight(230),
          width: SizeConfig.scaleWidth(335),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
          ),
          child: Image.asset(
            pathImage,
            height: SizeConfig.scaleHeight(230),
            width: SizeConfig.scaleWidth(335),
            fit: BoxFit.cover,
            isAntiAlias: true,
          ),
        ),
        Positioned(
          top: SizeConfig.scaleHeight(20),
          left: SizeConfig.scaleWidth(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TravelGuideText(
                text: title,
                fontSize: SizeConfig.scaleTextFont(18),
                fontWeight: FontWeight.w600,
                textColor: Colors.white,
              ),
              Row(
                children: [
                  Icon(
                    Icons.location_on,
                    color: Colors.white.withAlpha(200),
                    size: SizeConfig.scaleWidth(14),
                  ),
                  TravelGuideText(
                    text: subTitle,
                    fontSize: 14,
                    textColor: Colors.white,
                  ),
                ],
              ),
            ],
          ),
        ),
        Positioned(
          top: SizeConfig.scaleHeight(20),
          right: SizeConfig.scaleWidth(24),
          child: GestureDetector(
            onTap: ShowDetailsClick,
            child: TravelGuideText(
              text: showDetails,
              fontSize: SizeConfig.scaleTextFont(18),
              fontWeight: FontWeight.w500,
              textColor: Colors.white60,
            ),
          ),
        ),
      ],
    );
  }
}
