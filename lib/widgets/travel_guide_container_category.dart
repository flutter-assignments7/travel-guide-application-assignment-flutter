import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class TravelGuideContainerCategory extends StatelessWidget {

  final double containerMarginEnd;
  final VoidCallback onPressedIcon;
  final Widget icon;
  final String categoryTitle;


  TravelGuideContainerCategory({
    required this.containerMarginEnd,
    required this.onPressedIcon,
    required this.icon,
    required this.categoryTitle,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.scaleWidth(71),
      margin: EdgeInsetsDirectional.only(
        top: SizeConfig.scaleHeight(40),
        end: containerMarginEnd,
        bottom: SizeConfig.scaleHeight(28),
      ),
      height: SizeConfig.scaleHeight(100),
      child: Column(
        children: [
          Container(
            height: SizeConfig.scaleHeight(71),
            width: SizeConfig.scaleWidth(71),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(25),
              boxShadow: [
                BoxShadow(
                  // color:
                  color: Color(0xFF5B4DBC).withAlpha(40),
                  offset: Offset(0, 16),
                  blurRadius: 40,
                  // spreadRadius: 1,
                ),
              ],
            ),
            child: IconButton(
              onPressed: onPressedIcon,
              icon: icon,
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(12)),
          TravelGuideText(
            text: categoryTitle,
            fontSize: 10,
            fontWeight: FontWeight.bold,
          )
        ],
      ),
    );
  }
}
