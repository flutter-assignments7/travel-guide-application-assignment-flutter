import 'package:flutter/material.dart';
import 'package:travel_guide_app/screens/Offices/friends_office_screen.dart';
import 'package:travel_guide_app/screens/Offices/passport_screen.dart';
import 'package:travel_guide_app/screens/additional_travel_costs_screen.dart';
import 'package:travel_guide_app/screens/civil_status_screen.dart';
import 'package:travel_guide_app/screens/first_screen.dart';
import 'package:travel_guide_app/screens/bottom_navegationBar/main_screen.dart';
import 'package:travel_guide_app/screens/offices_coordination_screen.dart';
import 'package:travel_guide_app/screens/offices_travel_screen.dart';
import 'package:travel_guide_app/screens/ratifications_screen.dart';
import 'package:travel_guide_app/screens/sign_in_screen.dart';
import 'package:travel_guide_app/screens/sign_up_screen.dart';
import 'package:travel_guide_app/screens/travel_information_screen.dart';
import 'package:travel_guide_app/widgets/travel_guide_pageView_content.dart';

void main() => runApp(MainApp());

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/first_screen',
      routes: {
        '/first_screen' : (context) => FirstScreen(),
        '/sign_in_screen' : (context) => SignInScreen(),
        '/sign_up_screen' : (context) => SignUpScreen(),
        '/main_screen' : (context) => MainScreen(),
        '/offices_travel_screen' : (context) => OfficesTravelScreen(),
        '/offices_coordination_screen' : (context) => OfficesCoordinationScreen(),
        '/ratifications_screen' : (context) => RatificationsScreen(),
        '/travel_information_screen' : (context) => TravelInformationScreen(),
        '/civil_status_screen' : (context) => CivilStatusScreen(),
        '/friends_office_screen' : (context) => FriendsOfficeScreen(),
        '/passport_screen' : (context) => PassPortScreen(),
        '/additional_travel_costs_screen' : (context) => AdditionalTravelCostsScreen(),
      },
    );
  }
}
