import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_elevatedButton.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Color(0xFFF2F0FF),
      body: Stack(
        children: [
          Image.asset(
            'images/FirstS_bg.png',
            height: double.infinity,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          Padding(
            padding: EdgeInsetsDirectional.only(
              start: SizeConfig.scaleWidth(50),
              end: SizeConfig.scaleWidth(50),
              top: SizeConfig.scaleHeight(205),
            ),
            child: Container(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TravelGuideText(
                    text: 'Let\'s enjoy \n your Vacation!',
                    fontWeight: FontWeight.bold,
                    fontSize: SizeConfig.scaleTextFont(36),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: SizeConfig.scaleHeight(27),
                  ),
                  Container(
                    margin: EdgeInsetsDirectional.only(
                        start: SizeConfig.scaleWidth(28),
                        end: SizeConfig.scaleWidth(28)),
                    child: TravelGuideElevatedButton(
                      onPressed: (){
                        Navigator.pushNamed(context, '/sign_in_screen');
                      },
                      primaryColor: Color(0xFFFFA183),
                      buttonHeight: SizeConfig.scaleHeight(50),
                      // buttonWidth: SizeConfig.scaleWidth(219),
                      buttonText: 'Sign In',
                      fontSize: 15,
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.scaleHeight(20),
                  ),
                  Container(
                    margin: EdgeInsetsDirectional.only(
                        start: SizeConfig.scaleWidth(28),
                        end: SizeConfig.scaleWidth(28)),
                    child: TravelGuideElevatedButton(
                      onPressed: (){
                        Navigator.pushNamed(context, '/sign_up_screen');
                      },
                      primaryColor: Color(0xFFECC35A),
                      buttonHeight: SizeConfig.scaleHeight(50),
                      // buttonWidth: SizeConfig.scaleWidth(219),
                      buttonText: 'Sign Up ',
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
