import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_pageView_content.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class PassPortScreen extends StatelessWidget {
  const PassPortScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(50),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TravelGuideCard(
              trailingIcon: IconButton(
                onPressed: () {},
                icon: Image.asset(
                  'images/image_account.png',
                  width: SizeConfig.scaleWidth(70),
                  height: SizeConfig.scaleHeight(70),
                  fit: BoxFit.cover,
                ),
              ),
              leadingIcon: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: SizeConfig.scaleHeight(22),
                ),
              ),
            ),
            TravelGuideText(
              text: 'PassPort Details',
              fontSize: SizeConfig.scaleTextFont(28),
              fontWeight: FontWeight.bold,
              textColor: Color(0xFF5B4DBC),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            TravelGuidePageViewContent(
              pathImage: 'images/passport.jpg',
              height: 230,
              marginTopContainer1: 10,
              marginTopContainer2: 8,
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsetsDirectional.only(
                    bottom: SizeConfig.scaleHeight(20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    TravelGuideText(
                      text:
                          'أ.  الهوية + صور شخصية خلفية زرقاء عدد 4 + توكيل من نقابة المحامين + افادة مهنة ( إن وجد )',
                      textAlign: TextAlign.end,
                    ),
                    Container(
                      padding: EdgeInsetsDirectional.only(
                          end: SizeConfig.scaleWidth(40)),
                      child: TravelGuideText(
                        text:
                            'أ.1.  بخصوص افادة المهنة عبارة عن صورة عن شهادة الجامعة + افادة نقابة اصلية في حال كان مهنة لها نقابة + مزاولة المهنة الاصلية في حال مهنة تحتاج مزاولة + كتاب من مكان العمل الحالي',
                        textAlign: TextAlign.end,
                      ),
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: 'ب.  صلاحية الجواز 5 سنوات من تاريخ الاصدار',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                          'ج.  في حال وجود جواز قديم منتهي الصلاحية يمكن اخذ صورة عنه',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                          'د.  في حال وجود تعديل او تلف على جواز السفر تكون المعاملة بدل تالف ويجب تسليم الجواز مع الطلب لإلغائه من الداخلية',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                          'ه.  في حال فقدان الجواز إذا كان منتهي الصلاحية تكون معاملة اصدار جواز جديد عادي أما إذا كان ساري الصلاحية فيجب عمل اجراءات بدل فاقد نفس اجراءات الجواز العادية مع اضافة ( حلف يمين في نقابة المحامين + اعلان جريدة )',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: 'و.  يحتاج اصدار الجواز من اسبوع ل 10 ايام تقريبا',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                          'ز.  في حال بدل فاقد وبه صلاحية يحتاج من 9 شهور لسنة تقريبا',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                          'ح.  يعتبر الجواز بحاجة إلى تجديد اذا كانت الصلاحية المتبقية 6 شهور',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: ':ط.  في حال تجديد الجواز لشخص غير موجود في فلسطين',
                      textAlign: TextAlign.end,
                    ),
                    Container(
                      margin: EdgeInsetsDirectional.only(
                          top: SizeConfig.scaleHeight(10)),
                      padding: EdgeInsetsDirectional.only(
                          end: SizeConfig.scaleWidth(40)),
                      child: TravelGuideText(
                        text:
                            'ط.1.  التوجه للسفارة الفلسطينية في البلد المقيم فيها',
                        textAlign: TextAlign.end,
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      endIndent: 40,
                      indent: 30,
                    ),
                    Container(
                      padding: EdgeInsetsDirectional.only(
                          end: SizeConfig.scaleWidth(40)),
                      child: TravelGuideText(
                        text:
                            'ط.2.  عمل وكالة وارسالها إلى غزة لتكملة الاجراءات',
                        textAlign: TextAlign.end,
                      ),
                    ),
                    Divider(thickness: 1),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
