import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_pageView_content.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class FriendsOfficeScreen extends StatelessWidget {
  late BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(50),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TravelGuideCard(
              trailingIcon: IconButton(
                onPressed: () {},
                icon: Image.asset(
                  'images/image_account.png',
                  width: SizeConfig.scaleWidth(70),
                  height: SizeConfig.scaleHeight(70),
                  fit: BoxFit.cover,
                ),
              ),
              leadingIcon: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: SizeConfig.scaleHeight(22),
                ),
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(23)),
            TravelGuideText(
              text: 'Visa Details',
              fontSize: SizeConfig.scaleTextFont(20),
              fontWeight: FontWeight.bold,
            ),
            SizedBox(height: SizeConfig.scaleHeight(20)),
            Expanded(
              child: ListView(
                padding: EdgeInsetsDirectional.only(
                  top: SizeConfig.scaleHeight(15),
                  bottom: SizeConfig.scaleHeight(15),
                ),
                children: [
                  TravelGuidePageViewContent(
                    title: 'Turkey',
                    subTitle: 'Istanbul',
                    pathImage: 'images/turkey.jpg',
                    height: 220,
                    showDetails: 'Show Details...',
                    ShowDetailsClick: () {
                      showDetailsAboutVisa(
                        Column(
                          children: [
                            TravelGuideText(text: 'أ. جواز سفر ساري على سنة الاقل'),
                            Divider(thickness: 1,endIndent: 20,indent: 20,),
                            TravelGuideText(text: 'ب. صور شخصية خلفية بيضاء عدد 2 ( بحجم 6*6 )'),
                            Divider(thickness: 1,endIndent: 20,indent: 20,),
                            TravelGuideText(text: 'ج. افادة مهنة مصدقة'),
                            Divider(thickness: 1,endIndent: 20,indent: 20,),
                            TravelGuideText(text: 'د. حركة بنكية لاخر 3 شهور'),
                            Divider(thickness: 1,endIndent: 20,indent: 20,),
                            TravelGuideText(text: 'ه. مدة اصدارها خلال 10 ايام تأتي بصلاحية سنة من تاريخ اصدارها وتعطي اقامة مدة 30 يوم من تاريخ الدخول',textAlign: TextAlign.center,),
                          ],
                        )
                      );
                    },
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(15)),
                  TravelGuidePageViewContent(
                    title: 'Jordan',
                    subTitle: 'Oman',
                    pathImage: 'images/oman.jpg',
                    height: 220,
                    showDetails: 'Show Details...',
                    ShowDetailsClick: () {
                      showDetailsAboutVisa(
                          Column(
                            children: [
                              TravelGuideText(text: 'أ. صورة عن جواز السفر'),
                              Divider(thickness: 1,endIndent: 20,indent: 20,),
                              TravelGuideText(text: 'ب. صورة عن الهوية'),
                              Divider(thickness: 1,endIndent: 20,indent: 20,),
                              TravelGuideText(text: 'ج. صور شخصية عدد 2 خلفية بيضاء'),
                              Divider(thickness: 1,endIndent: 20,indent: 20,),
                              TravelGuideText(text: 'د. سبب دخول الاردن ( تقرير طبي / قبول جامعي / عقد عمل )'),
                              Divider(thickness: 1,endIndent: 20,indent: 20,),
                              TravelGuideText(text: 'ه. يحتاج اسبوعين للحصول على الموافقة او الرفض'),
                            ],
                          )
                      );
                    },
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(15)),
                  TravelGuidePageViewContent(
                    title: 'UAE',
                    subTitle: 'Dubai',
                    pathImage: 'images/UAE.jpg',
                    height: 220,
                    showDetails: 'Show Details...',
                    ShowDetailsClick: () {
                      showDetailsAboutVisa(
                          Column(
                            children: [
                              TravelGuideText(text: 'أ. تحتاج صورة ملونة واضحة عن جواز السفر'),
                              Divider(thickness: 1,endIndent: 20,indent: 20,),
                              TravelGuideText(text: 'ب. يتم اصدارها خلال 48 ساعة'),
                              Divider(thickness: 1,endIndent: 20,indent: 20,),
                              TravelGuideText(text: 'ج. صلاحيتها شهرين ومدة الاقامة 30 يوم او 90 يوم.'),
                            ],
                          )
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showDetailsAboutVisa(Widget widget) {
    showModalBottomSheet(
      context: _context,
      builder: (context) {
        return Container(
          padding: EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(20)),
          height: SizeConfig.scaleHeight(300),
          child: widget,
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),
      ),
      elevation: 4,
    );
  }
}
