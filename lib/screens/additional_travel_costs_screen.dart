import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_pageView_content.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class AdditionalTravelCostsScreen extends StatelessWidget {
  const AdditionalTravelCostsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(50),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TravelGuideCard(
              trailingIcon: IconButton(
                onPressed: () {},
                icon: Image.asset(
                  'images/image_account.png',
                  width: SizeConfig.scaleWidth(70),
                  height: SizeConfig.scaleHeight(70),
                  fit: BoxFit.cover,
                ),
              ),
              leadingIcon: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: SizeConfig.scaleHeight(22),
                ),
              ),
            ),
            TravelGuideText(
              text: 'Additional Travel Costs',
              fontSize: SizeConfig.scaleTextFont(28),
              fontWeight: FontWeight.bold,
              textColor: Color(0xFF5B4DBC),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            TravelGuidePageViewContent(
              pathImage: 'images/costs.jpg',
              height: 230,
              marginTopContainer1: 10,
              marginTopContainer2: 8,
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsetsDirectional.only(
                  bottom: SizeConfig.scaleHeight(20),
                  start: SizeConfig.scaleWidth(15),
                  end: SizeConfig.scaleWidth(15),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    TravelGuideText(
                      text:
                          'أ.  يتم دفع  10 ل 15 شيكل أجرة نقل لكل شخص من الصالة الخارجية حتى صالة المغادرة الفلسطينية الداخلية',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 3),
                    TravelGuideText(
                      text:
                          'ب.  في الصالة الفلسطينية : يتم دفع  67 شيكل شيك معبر',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 3),
                    TravelGuideText(
                      text: '..ملاحظات',
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      decorationStyle: TextDecorationStyle.solid,
                      decoration: TextDecoration.underline,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(15)),
                    TravelGuideText(
                      text:
                          'أ.  في الصالة الفلسطينية الأطفال تحت عمر أقل من سنتين لا يوجد عليهم دفع شيك معبر',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                          'ب.  في الصالة المصرية : يتم دفع 300 جنيه شيك معبر + ١٥٠ جنيه أجرة تحميل شنط  اذا كان شنط كبيرة يتم دفع 400 جنيه مصري لشركة هلا',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: 'ج.  في الصالة المصرية شيك العبور إلزامي على كل شخص حتى لو طفل عمر يوم وليس على كل جواز',
                      textAlign: TextAlign.end,
                    ),

                    Divider(thickness: 3),
                    TravelGuideText(
                      text: '..أجرة السيارة للنقل داخل مصر',
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      decorationStyle: TextDecorationStyle.solid,
                      decoration: TextDecoration.underline,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(15)),
                    TravelGuideText(
                      text:
                          'أ.  توصيل للعريش الراكب ب 200 جنيه مصري',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                          'ب.  توصيل داخل مصر او المطار 400 جنيه للراكب',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 3),
                    TravelGuideText(
                      text: '..تكاليف العودة لغزة',
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      decorationStyle: TextDecorationStyle.solid,
                      decoration: TextDecoration.underline,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(15)),
                    TravelGuideText(
                      text: 'أ.  في الصالة المصرية : يتم دفع 300 جنيه شيك معبر + 15 جنيه دمغة + 30 جنيه تذكرة باص + 10 جنيه تذكرة شنط باص + 150 ل 400 جنيه اجار تحميل الشنط لشركة هلا',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: 'ب.  في الصالة الفلسطينية  لا يوجد دفع للقادمين ويتم الحجر منزلي',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 3),
                    TravelGuideText(
                      text: 'غرامة التأخير التي يتم دفعها في الصالة المصرية',
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      decorationStyle: TextDecorationStyle.solid,
                      decoration: TextDecoration.underline,
                      textAlign: TextAlign.end,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(15)),
                    TravelGuideText(
                      text: 'أ.  غرامة من يوم ل 3 شهور قيمتها 1505 جنيه',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: 'ب.  غرامة من 3 شهور ل 6 شهور قيمتها 2005 جنيه',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: 'ج.  غرامة من 6 شهور ل 9 شهور قيمتها 2505 جنيه',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: 'د.  غرامة من 9 شهور ل سنة قيمتها 3005 جنيه',
                      textAlign: TextAlign.end,
                    ),
                    Container(
                      margin: EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(10)),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.yellowAccent,
                        border: Border.all(color: Colors.lime,width: 1),
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: TravelGuideText(
                        text: 'بمعنى أنه كل 3 شهور زيادة عليهم مخالفة 500 جنيه',
                        textAlign: TextAlign.end,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Divider(thickness: 3),
                    TravelGuideText(
                      text: '...ملاحظات هامة تابعة',
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      decorationStyle: TextDecorationStyle.solid,
                      decoration: TextDecoration.underline,
                      textAlign: TextAlign.end,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(15)),
                    TravelGuideText(
                      text: 'أ.  الأطفال عمر أقل من 16 سنة ليس عليهم غرامة تأخير',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: 'ب.  الرجال فوق 60 عام ليس عليهم غرامة تأخير',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
