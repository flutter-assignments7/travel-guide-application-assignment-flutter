import 'package:flutter/material.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class FavouriteScreen extends StatefulWidget {
  @override
  _FavouriteScreenState createState() => _FavouriteScreenState();
}

class _FavouriteScreenState extends State<FavouriteScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: TravelGuideText(
        text: 'Future Work...',
        fontSize: 26,
        textColor: Colors.black,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
