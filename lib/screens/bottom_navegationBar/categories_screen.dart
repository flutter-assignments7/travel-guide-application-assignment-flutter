import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:travel_guide_app/models/contact.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/categories_item.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class CategoriesScreen extends StatefulWidget {
  const CategoriesScreen({Key? key}) : super(key: key);

  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  List<Contact> _contacts = <Contact>[
    Contact(
      'Tourism and Travel Offices',
      'Visa Tourism and travel offices',
      Image.asset('images/icon_category.png'),
      '/offices_travel_screen',
    ),
    Contact(
      'Coordination Offices',
      'Coordination offices for entry into Egyptian territory',
      CircleAvatar(
        backgroundImage: AssetImage('images/crossing2.jpg'),
        radius: 30,
      ),
      '/offices_coordination_screen',
    ),
    Contact(
      'Ratifications',
      'Official documents for operation in external States must be certified by the State of issuance depending on the type of document',
      CircleAvatar(
        backgroundImage: AssetImage('images/gaza.jpg'),
        radius: 30,
      ),
      '/ratifications_screen',
    ),
    Contact(
      'Travel Information',
      'Information that every traveler should know',
      CircleAvatar(
        backgroundImage: AssetImage('images/travel.jpg'),
        radius: 30,
      ),
      '/travel_information_screen',
    ),
    Contact(
      'Civil Status',
      'Civil Status required by the traveller',
      CircleAvatar(
        backgroundImage: AssetImage('images/gaza.jpg'),
        radius: 30,
      ),
      '/civil_status_screen',
    ),
    Contact(
      'Additional Travel Costs',
      'Additional travel costs paid not related to Viza and Passport',
      CircleAvatar(
        backgroundImage: AssetImage('images/costs.jpg'),
        radius: 30,
      ),
      '/additional_travel_costs_screen',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(
        top: SizeConfig.scaleHeight(50),
        start: SizeConfig.scaleWidth(20),
        end: SizeConfig.scaleWidth(20),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TravelGuideCard(
            trailingIcon: IconButton(
              onPressed: () {},
              icon: Image.asset(
                'images/image_account.png',
                width: SizeConfig.scaleWidth(70),
                height: SizeConfig.scaleHeight(70),
                fit: BoxFit.cover,
              ),
            ),
            leadingIcon: IconButton(
              onPressed: () {},
              icon: Image.asset(
                'images/icon_search.png',
                width: SizeConfig.scaleWidth(20),
                height: SizeConfig.scaleHeight(20),
              ),
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(23)),
          TravelGuideText(
            text: 'Categories',
            fontSize: SizeConfig.scaleTextFont(28),
            fontWeight: FontWeight.bold,
          ),
          Expanded(
            child: ListView.builder(
              padding:
                  EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(29)),
              itemBuilder: (context, index) {
                Contact contact = _contacts.elementAt(index);
                return CategoriesItem(
                  leadingIcon: contact.widget,
                  title: contact.name,
                  subtitle: contact.details,
                  onTapGoToCategory: () {
                    Navigator.pushNamed(context, contact.route);
                  },
                );
              },
              itemCount: _contacts.length,
            ),
          ),
        ],
      ),
    );
  }
}
