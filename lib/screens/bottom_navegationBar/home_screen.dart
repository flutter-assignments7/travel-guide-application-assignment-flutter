import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_container_category.dart';
import 'package:travel_guide_app/widgets/travel_guide_pageView_content.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';
import 'package:travel_guide_app/widgets/travel_guide_textField.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(
        top: SizeConfig.scaleHeight(50),
        start: SizeConfig.scaleWidth(20),
        end: SizeConfig.scaleWidth(20),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TravelGuideCard(
            trailingIcon: IconButton(
              onPressed: () {},
              icon: Image.asset(
                'images/image_account.png',
                width: SizeConfig.scaleWidth(70),
                height: SizeConfig.scaleHeight(70),
                fit: BoxFit.cover,
              ),
            ),
            title: 'Hi ',
            titleName: 'Ibrahim Mohammed!',
            subTitle: 'Lit\'s discovery a New Adventure',
          ),
          SizedBox(height: SizeConfig.scaleHeight(30)),
          TravelGuideTextField(
            paddingBottom: SizeConfig.scaleHeight(18),
            paddingTop: SizeConfig.scaleHeight(18),
            paddingEnd: SizeConfig.scaleWidth(24),
            hintTextField: 'Search...',
            hintFontSize: SizeConfig.scaleTextFont(14),
            keyboardType: TextInputType.text,
            suffixIcon: IconButton(
              onPressed: () {},
              icon: Image.asset(
                'images/icon_search.png',
                width: SizeConfig.scaleWidth(20),
                height: SizeConfig.scaleHeight(20),
              ),
            ),
            // Icon(
            //   Icons.search_rounded,
            //   size: SizeConfig.scaleWidth(20),
            //   color: Colors.black,
            // ),
            fillColor: Color(0xFFF8F7FE),
          ),
          Row(
            children: [
              TravelGuideContainerCategory(
                containerMarginEnd: SizeConfig.scaleWidth(17),
                onPressedIcon: () {
                  Navigator.pushNamed(context, '/offices_travel_screen');
                },
                icon: Image.asset(
                  'images/icon_category.png',
                  width: double.infinity,
                  height: double.infinity,
                  fit: BoxFit.cover,
                ),
                categoryTitle: 'Travel',
              ),
              TravelGuideContainerCategory(
                containerMarginEnd: SizeConfig.scaleWidth(17),
                onPressedIcon: () {
                  Navigator.pushNamed(context, '/offices_coordination_screen');
                },
                icon: CircleAvatar(
                  backgroundImage: AssetImage('images/crossing2.jpg'),
                  radius: 30,
                ),
                categoryTitle: 'Coordination',
              ),
              TravelGuideContainerCategory(
                containerMarginEnd: SizeConfig.scaleWidth(17),
                onPressedIcon: () {
                  Navigator.pushNamed(context, '/ratifications_screen');
                },
                icon: Image.asset(
                  'images/gaza.jpg',
                    width: double.infinity,
                    height: double.infinity,
                    fit: BoxFit.cover,
                ),
                categoryTitle: 'Ratifications',
              ),
              TravelGuideContainerCategory(
                containerMarginEnd: SizeConfig.scaleWidth(0),
                onPressedIcon: () {
                  Navigator.pushNamed(context, '/civil_status_screen');
                },
                icon: Image.asset(
                  'images/gaza.jpg',
                    width: double.infinity,
                    height: double.infinity,
                    fit: BoxFit.cover,
                ),
                categoryTitle: 'Civil',
              ),
            ],
          ),
          TravelGuideText(
            text: 'Discover',
            fontSize: 28,
            fontWeight: FontWeight.bold,
            textAlign: TextAlign.start,
          ),
          SizedBox(height: SizeConfig.scaleHeight(10)),
          Container(
            margin: EdgeInsetsDirectional.only(
              end: SizeConfig.scaleWidth(95),
            ),
            child: TabBar(
              labelPadding: EdgeInsets.zero,
              indicatorSize: TabBarIndicatorSize.label,
              indicatorPadding: EdgeInsetsDirectional.only(
                  top: SizeConfig.scaleHeight(42),
                  end: SizeConfig.scaleWidth(15)),
              indicatorColor: Color(0xFF5B4DBC),
              indicatorWeight: 5,
              indicator: BoxDecoration(
                  color: Color(0xFF5B4DBC),
                  borderRadius: BorderRadius.circular(2)),
              unselectedLabelColor: Color(0xFFB7B7B7),
              labelColor: Color(0xFF5B4DBC),
              labelStyle: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w600,
                fontFamily: 'Poppins',
              ),
              unselectedLabelStyle: TextStyle(
                fontSize: 14,
                fontFamily: 'Poppins',
              ),

              tabs: [
                Tab(
                  text: 'Trend',
                ),
                Tab(
                  text: 'Featured',
                ),
                Tab(
                  text: 'Top Visit',
                ),
              ],
              // TravelGuidePageViewContent(),
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Expanded(
            child: TabBarView(
              children: [
                TravelGuidePageViewContent(
                  title: 'Turkey',
                  subTitle: 'Istanbul',
                  pathImage: 'images/turkey.jpg',
                  height: 220,
                ),
                TravelGuidePageViewContent(
                  title: 'Jordan',
                  subTitle: 'Oman',
                  pathImage: 'images/oman.jpg',
                  height: 220,
                ),
                TravelGuidePageViewContent(
                  title: 'UAE',
                  subTitle: 'Dubai',
                  pathImage: 'images/UAE.jpg',
                  height: 220,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
