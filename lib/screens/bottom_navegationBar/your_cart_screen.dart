import 'package:flutter/material.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class YourCartScreen extends StatefulWidget {
  const YourCartScreen({Key? key}) : super(key: key);

  @override
  _YourCartScreenState createState() => _YourCartScreenState();
}

class _YourCartScreenState extends State<YourCartScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: TravelGuideText(
          text: 'Future Work...',
          fontSize: 26,
          textColor: Colors.black,
          fontWeight: FontWeight.bold,
        ),
    );
  }
}
