import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:travel_guide_app/models/bn_screen.dart';
import 'package:travel_guide_app/screens/bottom_navegationBar/categories_screen.dart';
import 'package:travel_guide_app/screens/bottom_navegationBar/favourite_screen.dart';
import 'package:travel_guide_app/screens/bottom_navegationBar/home_screen.dart';
import 'package:travel_guide_app/screens/bottom_navegationBar/your_cart_screen.dart';
import 'package:travel_guide_app/utils/size_config.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;
  Color _colorSelected = Color(0xFF5B4DBC);
  Color _colorUnSelected = Color(0xFFB7B7B7);

  List<BottomNavigationScreen> _bnScreen = <BottomNavigationScreen>[
    BottomNavigationScreen(HomeScreen()),
    BottomNavigationScreen(CategoriesScreen()),
    BottomNavigationScreen(FavouriteScreen()),
    BottomNavigationScreen(YourCartScreen()),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: _bnScreen.elementAt(_currentIndex).widget,
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          elevation: 0,
          backgroundColor: Colors.transparent,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          selectedIconTheme: IconThemeData(
            color: _colorSelected,
          ),
          unselectedIconTheme: IconThemeData(
            color: _colorUnSelected,
          ),
          currentIndex: _currentIndex,
          onTap: (int selectedIndex) {
            setState(() {
              _currentIndex = selectedIndex;
            });
          },
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              backgroundColor: Colors.transparent,
              icon: SvgPicture.asset(
                'images/Home.svg',
                width: SizeConfig.scaleWidth(22),
                height: SizeConfig.scaleHeight(22),
                color: _currentIndex == 0 ? _colorSelected : _colorUnSelected,
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.transparent,
              icon: SvgPicture.asset(
                'images/Categories.svg',
                width: SizeConfig.scaleWidth(22),
                height: SizeConfig.scaleHeight(22),
                color: _currentIndex == 1 ? _colorSelected : _colorUnSelected,
              ),
              label: 'Categories',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.transparent,
              icon: SvgPicture.asset(
                'images/Favourite.svg',
                width: SizeConfig.scaleWidth(22),
                height: SizeConfig.scaleHeight(22),
                color: _currentIndex == 2 ? _colorSelected : _colorUnSelected,
              ),
              label: 'Favourite',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.transparent,
              icon: SvgPicture.asset(
                'images/yourCart.svg',
                width: SizeConfig.scaleWidth(22),
                height: SizeConfig.scaleHeight(22),
                color: _currentIndex == 3 ? _colorSelected : _colorUnSelected,
              ),
              label: 'Your Cart',
            ),
          ],
        ),
      ),
    );
  }
}
