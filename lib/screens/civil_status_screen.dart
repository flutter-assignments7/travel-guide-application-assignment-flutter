import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_categories_item.dart';
import 'package:travel_guide_app/widgets/travel_guide_pageView_content.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class CivilStatusScreen extends StatelessWidget {
  late BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(50),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TravelGuideCard(
              trailingIcon: IconButton(
                onPressed: () {},
                icon: Image.asset(
                  'images/image_account.png',
                  width: SizeConfig.scaleWidth(70),
                  height: SizeConfig.scaleHeight(70),
                  fit: BoxFit.cover,
                ),
              ),
              leadingIcon: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: SizeConfig.scaleHeight(22),
                ),
              ),
            ),
            TravelGuideText(
              text: 'Civil Status Details',
              fontSize: SizeConfig.scaleTextFont(28),
              fontWeight: FontWeight.bold,
              textColor: Color(0xFF5B4DBC),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            TravelGuidePageViewContent(
              pathImage: 'images/pal_civil.jpg',
              height: 230,
              marginTopContainer1: 10,
              marginTopContainer2: 8,
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            TravelGuideCategoriesItem(
              title: 'Palestinian Identity',
              subTitle: 'Information relating to Palestinian identity',
              onTap: () {
                showDetailsAboutVisa(Column(
                  children: [
                    TravelGuideText(
                      text: 'الهوية',
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      decorationStyle: TextDecorationStyle.solid,
                      decoration: TextDecoration.underline,
                    ),
                    TravelGuideText(
                      text:
                          'أ. صور شخصية خلفية زرقاء مع شعار الداخلية عدد 4 + صورة هوية الوالد / صورة عن الهوية القديمة في حال التجديد',
                      textAlign: TextAlign.center,
                    ),
                    Divider(
                      thickness: 1,
                      endIndent: 20,
                      indent: 20,
                    ),
                    TravelGuideText(
                      text:
                          'ب. يتم تعبئة الطلب وتقديمه للداخلية ويجب حضور شخص معرف لاستلام الهوية',
                      textAlign: TextAlign.center,
                    ),
                    Divider(
                      thickness: 1,
                      endIndent: 20,
                      indent: 20,
                    ),
                    TravelGuideText(
                      text: 'ج. صلاحية الهوية 10 سنوات من تاريخ الاصدار',
                      textAlign: TextAlign.center,
                    ),
                    Divider(
                      thickness: 1,
                      endIndent: 20,
                      indent: 20,
                    ),
                    TravelGuideText(
                      text: 'د. يتم اصدارها في نفس اليوم',
                      textAlign: TextAlign.center,
                    ),
                  ],
                ));
              },
              pathImage: 'images/pal1.jpg',
            ),
            TravelGuideCategoriesItem(
              title: 'Palestinian Birth Certificate',
              subTitle: 'Information relating to Palestinian Birth Certificate',
              onTap: () {
                showDetailsAboutVisa(Column(
                  children: [
                    TravelGuideText(
                      text: 'استخراج شهادة ميلاد',
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      decorationStyle: TextDecorationStyle.solid,
                      decoration: TextDecoration.underline,
                    ),
                    TravelGuideText(text: 'أ. صورة عن هوية الوالد'),
                    Divider(
                      thickness: 1,
                      endIndent: 20,
                      indent: 20,
                    ),
                    TravelGuideText(
                        text:
                            'ب. صورة عن شهادة الميلاد القديمة في حال التجديد'),
                    Divider(
                      thickness: 1,
                      endIndent: 20,
                      indent: 20,
                    ),
                    TravelGuideText(text: 'ج. عمل وكالة من نقابة المحامين'),
                    Divider(
                      thickness: 1,
                      endIndent: 20,
                      indent: 20,
                    ),
                    TravelGuideText(text: 'د. يتم اصدارها في نفس اليوم'),
                  ],
                ));
              },
              pathImage: 'images/pal2.jpg',
            ),
            TravelGuideCategoriesItem(
              title: 'Certificate of Good Conduct',
              subTitle: 'Information relating to Certificate of Good Conduct',
              onTap: () {
                showDetailsAboutVisa(Column(
                  children: [
                    TravelGuideText(
                      text: 'حسن سير وسلوك',
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      decorationStyle: TextDecorationStyle.solid,
                      decoration: TextDecoration.underline,
                    ),
                    TravelGuideText(
                        text: 'أ. صورة الهوية + صورة شخصية خلفية بيضاء'),
                    Divider(
                      thickness: 1,
                      endIndent: 20,
                      indent: 20,
                    ),
                    TravelGuideText(
                        text: 'ب. يتم اصدارها في نفس اليوم من الداخلية'),
                  ],
                ));
              },
              pathImage: 'images/pal3.jpg',
            ),
          ],
        ),
      ),
    );
  }

  void showDetailsAboutVisa(Widget widget) {
    showModalBottomSheet(
      context: _context,
      builder: (context) {
        return Container(
          padding: EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(20)),
          height: SizeConfig.scaleHeight(300),
          child: widget,
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),
      ),
      elevation: 4,
    );
  }
}
