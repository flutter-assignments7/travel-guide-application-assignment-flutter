import 'package:flutter/material.dart';
import 'package:travel_guide_app/models/contact.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/categories_item.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_categories_item.dart';
import 'package:travel_guide_app/widgets/travel_guide_pageView_content.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class RatificationsScreen extends StatefulWidget {
  const RatificationsScreen({Key? key}) : super(key: key);

  @override
  _RatificationsScreenState createState() => _RatificationsScreenState();
}

class _RatificationsScreenState extends State<RatificationsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(50),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TravelGuideCard(
              trailingIcon: IconButton(
                onPressed: () {},
                icon: Image.asset(
                  'images/image_account.png',
                  width: SizeConfig.scaleWidth(70),
                  height: SizeConfig.scaleHeight(70),
                  fit: BoxFit.cover,
                ),
              ),
              leadingIcon: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: SizeConfig.scaleHeight(22),
                ),
              ),
            ),
            TravelGuideText(
              text: 'Ratifications Details',
              fontSize: SizeConfig.scaleTextFont(28),
              fontWeight: FontWeight.bold,
              textColor: Color(0xFF5B4DBC),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            TravelGuidePageViewContent(
              pathImage: 'images/ratifications.jpg',
              height: 230,
              marginTopContainer1: 10,
              marginTopContainer2: 8,
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),

            TravelGuideCategoriesItem(
              title: 'University Degree',
              subTitle: 'Ratifications relating to university degrees',
              onTap: (){
                showDetailsAboutVisa(
                    Column(
                      children: [
                        TravelGuideText(
                          text: 'الشهادة الجامعية',
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          decorationStyle: TextDecorationStyle.solid,
                          decoration: TextDecoration.underline,
                        ),
                        TravelGuideText(text: 'أ. تصديق من الجامعة'),
                        Divider(thickness: 1,endIndent: 20,indent: 20,),
                        TravelGuideText(text: 'ب. تصديق من وزارة التربية و التعليم ( ختم الوزير / عورتاني)'),
                        Divider(thickness: 1,endIndent: 20,indent: 20,),
                        TravelGuideText(text: 'ج. تصديق من الخارجية في رام الله '),
                      ],
                    )
                );
              },
              pathImage: 'images/ratifications_university.jpg',
            ),
            TravelGuideCategoriesItem(
              title: 'Court Papers',
              subTitle: 'Ratifications relating to Court Papers',
              onTap: (){
                showDetailsAboutVisa(
                    Column(
                      children: [
                        TravelGuideText(
                          text: 'الأوراق الصادرة من المحكمة',
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          decorationStyle: TextDecorationStyle.solid,
                          decoration: TextDecoration.underline,
                        ),
                        TravelGuideText(text: 'أ. تصديق المحكمة'),
                        Divider(thickness: 1,endIndent: 20,indent: 20,),
                        TravelGuideText(text: 'ب. مصادقة من الشيخ في غزة'),
                        Divider(thickness: 1,endIndent: 20,indent: 20,),
                        TravelGuideText(text: 'ج. مصادقة من قاضي القضاة والخارجية في رام الله'),

                      ],
                    )
                );
              },
              pathImage: 'images/certification.jpg',
            ),
            TravelGuideCategoriesItem(
              title: 'Official Agencies',
              subTitle: 'Ratifications relating to university degrees',
              onTap: (){
                showDetailsAboutVisa(
                    Column(
                      children: [
                        TravelGuideText(
                          text: 'الوكالات الرسمية',
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          decorationStyle: TextDecorationStyle.solid,
                          decoration: TextDecoration.underline,
                        ),
                        TravelGuideText(text: 'أ. تصديق من محامي'),
                        Divider(thickness: 1,endIndent: 20,indent: 20,),
                        TravelGuideText(text: 'ب. تصديق من نقابة المحامين بغزة'),
                        Divider(thickness: 1,endIndent: 20,indent: 20,),
                        TravelGuideText(text: 'ج. تصديق من عدل وخارجية رام الله'),

                      ],
                    )
                );
              },
              pathImage: 'images/pal.jpg',
            ),
            TravelGuideCategoriesItem(
              title: 'Papers issued by States other than Palestine',
              subTitle: 'Ratifications relating to university degrees Papers issued by States other than Palestine',
              onTap: (){
                showDetailsAboutVisa(
                    Column(
                      children: [
                        TravelGuideText(
                          text: 'الاوراق الصادرة من الدول غير فلسطين',
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          decorationStyle: TextDecorationStyle.solid,
                          decoration: TextDecoration.underline,
                        ),
                        TravelGuideText(text: 'أ. تصديق من جهة الاصدار'),
                        Divider(thickness: 1,endIndent: 20,indent: 20,),
                        TravelGuideText(text: 'ب. تصديق من السفارة الفلسطينية في الدولة'),
                        Divider(thickness: 1,endIndent: 20,indent: 20,),
                        TravelGuideText(text: 'ج. تصديق من خارجية الدولة'),

                      ],
                    )
                );
              },
              pathImage: 'images/pal_out2.jpg',
            ),
          ],
        ),
      ),
    );
  }

  void showDetailsAboutVisa(Widget widget) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          padding: EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(20)),
          height: SizeConfig.scaleHeight(300),
          child: widget,
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),
      ),
      elevation: 4,
    );
  }

}
