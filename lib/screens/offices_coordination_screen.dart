import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_pageView_content.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class OfficesCoordinationScreen extends StatefulWidget {
  const OfficesCoordinationScreen({Key? key}) : super(key: key);

  @override
  _OfficesCoordinationScreenState createState() =>
      _OfficesCoordinationScreenState();
}

class _OfficesCoordinationScreenState extends State<OfficesCoordinationScreen> {
  late TapGestureRecognizer _tapGestureRecognizer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _tapGestureRecognizer = TapGestureRecognizer()..onTap = showDetails;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        title: TravelGuideText(
          text: 'Coordination Offices',
          fontSize: SizeConfig.scaleTextFont(20),
          fontWeight: FontWeight.bold,
        ),
        leading: IconButton(
          onPressed: () {
            setState(() {
              Navigator.pop(context);
            });
          },
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.black,
            size: SizeConfig.scaleWidth(24),
          ),
        ),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(15),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              // padding: EdgeInsetsDirectional.only(
              //   end: SizeConfig.scaleWidth(15),
              //   start: SizeConfig.scaleWidth(15),
              // ),
              width: double.infinity,
              height: SizeConfig.scaleHeight(180),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: (Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: SizeConfig.scaleHeight(30),
                    child: Image.asset(
                      'images/note.png',
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  TravelGuideText(
                    text: 'ملاحظة',
                    textAlign: TextAlign.end,
                    fontSize: 28,
                    textColor: Colors.red,
                    fontWeight: FontWeight.bold,
                  ),
                  TravelGuideText(
                    text:
                        'الشركة القانونية التي تعمل في هذا المجال هي شركة هلا المصرية ولها فرعين الأول في غزة في شارع الجلاء والثاني في رفح دوار الشهداء (النجمة سابقا)',
                    textAlign: TextAlign.center,
                    fontWeight: FontWeight.w600,
                  ),
                ],
              )),
            ),
            SizedBox(height: SizeConfig.scaleHeight(10)),
            TravelGuidePageViewContent(
              pathImage: 'images/hala.jpg',
              height: 230,
              marginTopContainer1: 10,
              marginTopContainer2: 8,
            ),
            TravelGuideText(
              text: '..ملاحظات',
              fontSize: 18,
              fontWeight: FontWeight.bold,
              decorationStyle: TextDecorationStyle.solid,
              decoration: TextDecoration.underline,
            ),
            TravelGuideText(
              text:
                  'يتم عمل تنسيق للدخول إلى الاراضي المصرية وذلك لضمان عدم الارجاع على المعبر',
              textAlign: TextAlign.center,
            ),
            Divider(thickness: 1),
            TravelGuideText(
              text:
                  'ما دون ذلك تكون تنسيقات بطرق غير رسمية وتكون اقل تكلفة وبدون خدمات فقط تسجيل الاسم في كشف السفر',
              textAlign: TextAlign.center,
            ),
            Divider(thickness: 1),
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  width: 1,
                  color: Colors.black,
                ),
              ),
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: 'لمعرفة الخدمات التي يقدمها مكتب هلا ',
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'Poppins',
                    fontSize: SizeConfig.scaleTextFont(18),
                  ),
                  children: [
                    TextSpan(
                      recognizer: _tapGestureRecognizer,
                      text: 'اضغط هنا',
                      style: TextStyle(
                        color: Colors.lightBlue,
                        fontSize: SizeConfig.scaleTextFont(18),
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showDetails() {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          // alignment: Alignment.centerRight,
          padding: EdgeInsetsDirectional.only(
            top: SizeConfig.scaleHeight(20),
            start: SizeConfig.scaleWidth(15),
            end: SizeConfig.scaleWidth(15),
          ),
          height: SizeConfig.scaleHeight(300),
          child: Column(
            children: [
              TravelGuideText(
                text: 'أ. يتم التسجيل فيها قبل موعد السفر ب 72 ساعة على الاقل ',
                textAlign: TextAlign.center,
              ),
              Divider(
                thickness: 1,
                endIndent: 20,
                indent: 20,
              ),
              TravelGuideText(
                text:
                    'ب. يتم التأكد من الحالة الامنية للمسافر ( في حال رفض امني   او رفض قبول للسفر)',
                textAlign: TextAlign.center,
              ),
              Divider(
                thickness: 1,
                endIndent: 20,
                indent: 20,
              ),
              TravelGuideText(
                text:
                    'ج. اجراء تسهيلات للسفر عن طريق اضافة الاسم في كشوفات السفر دون انتظار + دفع التكاليف والمواصلات من غزة للمعبر وشيك العبور الفلسطيني وشيك العبور المصري مع استقبال في صالة كبار الزوار وتقديم ضيافة والمواصلات من الصالة المصرية حتى القاهرة',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),
      ),
      elevation: 4,
    );
  }
}
