import 'package:flutter/material.dart';
import 'package:travel_guide_app/models/contact_offices.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class OfficesTravelScreen extends StatefulWidget {
  const OfficesTravelScreen({Key? key}) : super(key: key);

  @override
  _OfficesTravelScreenState createState() => _OfficesTravelScreenState();
}

class _OfficesTravelScreenState extends State<OfficesTravelScreen> {
  List<ContactOffices> _contantOffises = <ContactOffices>[
    ContactOffices(
      'تفاصيل جواز السفر',
      CircleAvatar(
        backgroundImage: AssetImage('images/passport.jpg'),
        radius: 30,
      ),
      '/passport_screen',
    ),
    ContactOffices(
      'مكتب الأصدقاء للسياحة والسفر',
      CircleAvatar(
        backgroundImage: AssetImage('images/alasdeqa.jpg'),
        radius: 30,
      ),
      '/friends_office_screen',
    ),
    ContactOffices(
      'مكتب مشتهى للسياحة والسفر',
      CircleAvatar(
        backgroundImage: AssetImage('images/mushtaha.png'),
        radius: 30,
      ),
      '/',
    ),
    ContactOffices(
      'مكتب رفح للسياحة والسفر',
      CircleAvatar(
        backgroundImage: AssetImage('images/rafah.jpg'),
        radius: 30,
      ),
      '/',
    ),
    ContactOffices(
      'مكتب القدس الشرفا للسياحة والسفر',
      CircleAvatar(
        backgroundImage: AssetImage('images/alshurafa.jpg'),
        radius: 30,
      ),
      '/',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(50),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TravelGuideCard(
              trailingIcon: IconButton(
                onPressed: () {},
                icon: Image.asset(
                  'images/image_account.png',
                  width: SizeConfig.scaleWidth(70),
                  height: SizeConfig.scaleHeight(70),
                  fit: BoxFit.cover,
                ),
              ),
              leadingIcon: IconButton(
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: SizeConfig.scaleHeight(22),
                ),
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(23)),
            Row(
              children: [
                TravelGuideText(
                  text: 'Offices Travel',
                  fontSize: SizeConfig.scaleTextFont(20),
                  fontWeight: FontWeight.bold,
                ),
                Spacer(),
                TravelGuideText(
                  text: 'Country',
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  textColor: Color(0xFFB7B7B7),
                ),
                PopupMenuButton<String>(
                  // onSelected: (String selectedItem) {
                  //   switch (selectedItem) {
                  //     case 'About':
                  //       print("About me :: $selectedItem");
                  //       break;
                  //     case 'Ibrahim':
                  //       print("I am :: $selectedItem");
                  //       break;
                  //   }
                  // },
                  icon: Icon(
                    Icons.more_horiz,
                    color: Color(0xFF5B4DBC),
                    size: SizeConfig.scaleWidth(25),
                  ),
                  offset: Offset(0, 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.only(
                      bottomStart: Radius.circular(10),
                      bottomEnd: Radius.circular(10),
                    ),
                  ),
                  itemBuilder: (context) {
                    return [
                      PopupMenuItem(
                        child: Text('Gaza'),
                        // value: 'About',
                        enabled: false,
                      ),
                      PopupMenuItem(
                        child: Text('Nuseirat'),
                        // value: 'Ibrahim',
                        enabled: false,
                      ),
                      PopupMenuItem(
                        child: Text('Khanyons'),
                        // value: 'About',
                        enabled: false,
                      ),
                      PopupMenuItem(
                        child: Text('Rafah'),
                        // value: 'About',
                        enabled: false,
                      ),
                    ];
                  },
                ),
              ],
            ),
            Expanded(
              child: ListView.builder(
                padding:
                    EdgeInsetsDirectional.only(top: SizeConfig.scaleHeight(29)),
                itemBuilder: (context, index) {
                  ContactOffices contact = _contantOffises.elementAt(index);
                  return TravelGuideCard(
                    leadingIcon: contact.widget,
                    title: contact.name,
                    pushTile: () {
                      Navigator.pushNamed(context, contact.route);
                    },
                    trailingIcon: Icon(Icons.arrow_forward_ios),
                  );
                },
                itemCount: _contantOffises.length,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
