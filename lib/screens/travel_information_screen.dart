import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_card.dart';
import 'package:travel_guide_app/widgets/travel_guide_pageView_content.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';

class TravelInformationScreen extends StatelessWidget {
  const TravelInformationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(50),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TravelGuideCard(
              trailingIcon: IconButton(
                onPressed: () {},
                icon: Image.asset(
                  'images/image_account.png',
                  width: SizeConfig.scaleWidth(70),
                  height: SizeConfig.scaleHeight(70),
                  fit: BoxFit.cover,
                ),
              ),
              leadingIcon: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: SizeConfig.scaleHeight(22),
                ),
              ),
            ),
            TravelGuideText(
              text: 'Travel Information',
              fontSize: SizeConfig.scaleTextFont(28),
              fontWeight: FontWeight.bold,
              textColor: Color(0xFF5B4DBC),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            TravelGuidePageViewContent(
              pathImage: 'images/travel.jpg',
              height: 230,
              marginTopContainer1: 10,
              marginTopContainer2: 8,
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsetsDirectional.only(
                    bottom: SizeConfig.scaleHeight(20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    TravelGuideText(
                      text:
                      'أ.  لا يسمح للسفر للأطفال دون سن 18 سنة بدون موافقة ولي الامر',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text: 'ب.  لا يسمح للمرأة السفر مع اولادها بدون موافقة الزوج والد الاطفال من خلال عمل عدم ممانعة من الزوج في المحكمة',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                      'ج.  لا يسمح للسفر على جواز سفر تالف او عليه خربشة او متبقي من صلاحيته 6 شهور',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                      'د.  يمكن السفر على جواز سفر منتهى يتم تجديده من داخلية غزة لمصر فقط',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                    TravelGuideText(
                      text:
                      'ه.  لا يتم التعامل بالجواز الفلسطيني المصفر في المعبر المصري ( جواز يتم اصداره لسفرة واحدة لمن ليس لديهم هوية)',
                      textAlign: TextAlign.end,
                    ),
                    Divider(thickness: 1),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
