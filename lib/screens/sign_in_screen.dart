import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:travel_guide_app/utils/size_config.dart';
import 'package:travel_guide_app/widgets/travel_guide_elevatedButton.dart';
import 'package:travel_guide_app/widgets/travel_guide_text.dart';
import 'package:travel_guide_app/widgets/travel_guide_textField.dart';

class SignInScreen extends StatefulWidget {

  @override
  _SignInScreenState createState() => _SignInScreenState();
}


class _SignInScreenState extends State<SignInScreen> {

  late TextEditingController _emailTextEditingController;
  late TextEditingController _passwordTextEditingController;

  String? _emailError;
  String? _passwordError;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
  }



  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          'images/SignIn_bg.png',
          height: double.infinity,
          width: double.infinity,
          fit: BoxFit.cover,
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            leading: IconButton(
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: Colors.black,
              ),
            ),
            title: Row(
              children: [
                Spacer(),
                TravelGuideText(
                  text: 'Sign In',
                  fontWeight: FontWeight.w500,
                ),
              ],
            ),
          ),
          body: Padding(
            padding: EdgeInsetsDirectional.only(
              top: SizeConfig.scaleHeight(150),
              start: SizeConfig.scaleWidth(34),
              end: SizeConfig.scaleWidth(34),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TravelGuideText(
                    text: 'Sign In',
                    fontSize: SizeConfig.scaleTextFont(36),
                    fontWeight: FontWeight.bold,
                  ),
                  TravelGuideText(
                    text: 'Login to start using app',
                    fontSize: SizeConfig.scaleTextFont(18),
                    fontWeight: FontWeight.w300,
                    textColor: Color(0xFF707070),
                  ),
                  SizedBox(
                    height: SizeConfig.scaleHeight(80),
                  ),
                  TravelGuideTextField(
                    textEditingController: _emailTextEditingController,
                    keyboardType: TextInputType.emailAddress,
                    textError: _emailError,
                    hintTextField: 'Your Email',
                    textSize: SizeConfig.scaleTextFont(15),
                    hintFontSize: SizeConfig.scaleTextFont(15),
                    borderWidth: SizeConfig.scaleWidth(1),
                  ),
                  SizedBox(
                    height: SizeConfig.scaleHeight(15),
                  ),
                  TravelGuideTextField(
                    textEditingController: _passwordTextEditingController,
                    keyboardType: TextInputType.visiblePassword,
                    textError: _passwordError,
                    hintTextField: 'Your Password',
                    obscureText: true,
                    textSize: SizeConfig.scaleTextFont(15),
                    hintFontSize: SizeConfig.scaleTextFont(15),
                    borderWidth: SizeConfig.scaleWidth(1),
                    suffixIcon: GestureDetector(
                      onTap: () {},
                      child: Padding(
                        padding: EdgeInsetsDirectional.only(
                            end: SizeConfig.scaleWidth(23)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TravelGuideText(
                              text: 'forget?',
                              textAlign: TextAlign.center,
                              textColor: Color(0xFFB7B7B7),
                              fontSize: SizeConfig.scaleTextFont(14),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(30),),
                  TravelGuideElevatedButton(
                    onPressed: (){
                      performLogin();
                    },
                    buttonHeight: SizeConfig.scaleHeight(50),
                    buttonText: 'Sign In',
                    fontSize: 16,
                    primaryColor: Color(0xFFFFA183),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void performLogin() {
    if (checkData()) {
      //TODO:NAVIGATE TO HOME SCREEN
    }
  }

  //TODO:CHECK IF FIELDS HAVE DATA
  bool checkData() {
    if (_emailTextEditingController.text.isNotEmpty &&
        _passwordTextEditingController.text.isNotEmpty) {
      checkFieldsError();
      showMessage('Login Success');
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pushNamedAndRemoveUntil(context, '/main_screen', (route) => false);
      });
      return true;
    }
    checkFieldsError();
    showMessage('Please, Enter required data', error: true);
    return false;
  }

  void checkFieldsError() {
    setState(() {
      _emailError = _emailTextEditingController.text.isEmpty
          ? 'Enter Email Address'
          : null;
      _passwordError =
      _passwordTextEditingController.text.isEmpty ? 'Enter Password' : null;
    });
  }

//TODO:SHOW ERROR MESSAGE IN CASE OF ERROR DATA
  void showMessage(String message, {bool error = false}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        content: TravelGuideText(
          text: message,
          textColor: Colors.white,
        ),
        backgroundColor: error ? Colors.red : Colors.green,
      ),
    );
  }
}
