import 'package:flutter/material.dart';

class BottomNavigationScreen{

  Widget _widget;

  BottomNavigationScreen(this._widget);

  Widget get widget => _widget;

  set widget(Widget value) {
    _widget = value;
  }
}