import 'package:flutter/material.dart';

class ContactOffices{
  String _name;
  Widget _widget;
  String route;

  ContactOffices(this._name, this._widget, this.route);

  Widget get widget => _widget;

  set widget(Widget value) {
    _widget = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }
}