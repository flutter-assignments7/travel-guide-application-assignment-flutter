import 'package:flutter/material.dart';

class Contact{
  String _name;
  Widget _widget;
  String route;
  String _details;

  Contact(this._name, this._details,this._widget,this.route);


  Widget get widget => _widget;

  set widget(Widget value) {
    _widget = value;
  }

  String get details => _details;

  set details(String value) {
    _details = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }

}